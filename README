
The program transforms an operator expression into an expression in terms of integrals and amplitudes.

MOVED TO https://github.com/fkfest/quantwo

COMPILATION
------------

tar -xvf quantwo.tgz
cd quantwo
make

Generates an executable "quantwo"

MAKE COMMANDS:

1) make : compiles the program
2) make clean : removes all object files and executables
3) make veryclean : removes all object files, executables, *.pdf, *.aux, *.dvi, *.ps, *.log, *~
4) make equation : generates "equation.pdf" from "input.q2"
5) make depend : analyzes dependencies in src
6) make base : creates working environment (see the next paragraph)

by default rational numbers from boost-library are used as factors. If you don't have boost installed or if you
prefer double precision numbers, you can switch it off in Makefile by commenting out line
"CFLAGS := $(CFLAGS) -D _RATIONAL" 

EXECUTION
---------

To run the program:
"quantwo <input-file> [<output-file>]"

If an <output-file> was not given, the output will be written in <input-file without extension>.tex

The output file can be transformed to PDF using latex:
"pdflatex equation.tex"
(NOTE: the output filename has to be "input.tex" or should be changed in file equation.tex)
The equation can of course also be copied to some external TEX file. The definitions can be found 
in definitions.tex)

You can create a working environment by creating a symbolic link to the Makefile and running "make base":
mkdir PATH_TO_DIR/WORKING_DIR
cd PATH_TO_DIR/WORKING_DIR
ln -s PATH_TO_Q2_DIR/Makefile .
make base

It creates symbolic links to all needed files from quantwo directory. 
With "make equation" you can create then equation.pdf from "input.q2"-file
With "make equation out=<filename>" you can create <filename>.pdf from "input.q2"-file
With "make equation in=<filename>" you can create equation.pdf from "<filename>.q2"-file

COMMAND-LINE PARAMETERS

--help (-h) print this README
--verbose [level] (-v [level]) be chatty - print more 

OPTIONS
-------

Various options can be changed in a config file "params.reg" (including a definition of simple new commands (set="newcommand"))

CHANGING OPTIONS IN INPUT

one can change options in input-file using <set>:<name>=<value>[,<name>=<value>[,...]]
e.g., syntax:beq="\beqa",eeq="\eeqa" 

INPUT FILE
----------

The input file has a Latex form.
The equation has to be surrounded by \begin(equation) (or \beq) and \end(equation) (or \eeq) (syntax:beq,eeq).
The comments can be done using %.

At this stage the following mathematical operators and latex commands will be recognized:
(, [, ), ], {, }, +, -, <number>, \frac{<number>}{<number>}, \half, < <bra> |, | <ket> >, \op <operator>, \mu_<integer>,
\dagger, \dg, \tnsr <tensor>, \sum_{<excitation>[,<excitation>[,...]]}, \newcommand{}{} 

Following latex commands will be ignored:
\left, \right, \lk, \rk, &, \\ (syntax:skipop)

All not recognized symbols will be ignored and a warning will be written.

NEWCOMMAND:

a simple version of \newcommand is implemented, which can be used from input file (one per line):
\newcommand{}{}
e.g. \newcommand{\U}{(\op T_1 + \op T_2)} -> all \U will be replaced by (\op T_1 + \op T_2)

NEWOPERATOR:

with \newoperator (or \newop) command one can define custom operators: \newop{}{}
e.g. \newop{H}{\op F + \op W} -> hamiltonian
     \newop{T}{\op T_1 + \op T_2} 

OPERATORS:

1) Parts of Hamiltonian: \op H, \op F, \op W and \op X (1-electron perturbation) (hamilton:fock hamilton:flucpot hamilton:perturbation)
2) Excitation operators: \op \tau_{<excitation>}, where <excitation> can be, e.g., \mu_1, \nu_3, \mu_2^{-1} (non-conserving) etc
3) Cluster operators and intermediates: \op <name of operator>_<excitation class>^<addition to name and/or \dagger>, e.g., 
                                        \op T_2, \op U_3, \op S_4^{X}, \op Z^{bla}_1
   <name of operator> must not match name of a part of Hamiltonian (1)!
Deexcitation operators have to be marked with ^\dagger (or ^\dg), e.g., \op T_2^\dg, \op T_4^{X\dagger}
4) Non-conserving operators: \op <name of operator>_<excitation class>^<addition to name and/or \dagger and change in particle number>, e.g., 
    \op T_2^{+1} (2 occ.orbs and 3 virt.orbs), \op T_1^{-1} (1 occ.orbs and 0 virt. orbs)
5) (De)Excitation operators with explicitly given types of orbitals: 
        \op <name of operator>_<excitation class_<orbitals to annihilate>^<orbitals to create>>^<addition to name and/or \dagger>, e.g.,
    \op T_{2_{ii}^{aa}} (usual excitation operator), \op T_{2_{iu}^{ua}} (annihilate electrons in closed-shell and active space and create in active and virtual space)

BRA/KET:

bra and ket can be  
1) Reference state: 0, \Phi_0 or HF (all have the same meaning) (syntax:ref)
2) Excited determinant:  <excitation> (like in excitation operators ), e.g., \mu_1, \nu_3, etc
3) Explicitly given excited determinant: \Phi^<set of indices>_<set of indices>, e.g. \Phi^{ab}_{ii_{1}}, \Phi_{abij}, etc (syntax:csf)

TENSORS:

1) Tensor with excitation indices:  \tnsr <name of tensor>_<excitation>^<addition to name> (command:tensor)
    e.g., \tnsr \Lambda_{\mu_2}
  <excitation> should match one of <excitation>s in Excitation operators or Excited determinant
2) Number : \tnsr <name of tensor>^<addition to name>   , e.g., \tnsr \omega^X

SUM:

1) Sum over excitations: \sum_{<excitation>[,<excitation>[,...]]} 
    e.g., \sum_{\mu_2}
  <excitation> should match one of <excitation>s in Excitation operators or Excited determinant
  
CONNECTIONS

Connection (disconnection) of an expression can be set using "_C" ("_D").
E.g., (\op T_2^\dg \op W \op T_2 \op T_1)_C means that the whole expression has to be connected.
(\op T_2^\dg \op W \op T_2 \op T_1)_D means that a part of the equation has to be disconnected.
The following equality applies:
(...) = (...)_C + (...)_D

If connection sign is written after ket (|...>_C), the expression inside of bra/ket has to be connected 
(as in usual Coupled Cluster).

NO NORMAL ORDERING

to switch off the normal ordering set prog,wick=2,noorder=2
noorder=1: only Hamiltonian is not ordered.
In order to replace h_pq in the final expressions by f_pq set prog,usefock=1

MULTIREFERENCE

to have internally contracted expressions with active orbitals set prog,multiref=1
contracted excitation operators can be activated by setting prog,contrexcop=1 (default now)


EXAMPLE

Energy expression:
\beq
<0|\op W (\op T_2+\frac{1}{2}\op T_1 \op T_1) |0>
\eeq

LCCD amplitude equation:
\beq
<\mu_2|(\op F + \op W) (1+\op T_2) |0>_C
\eeq

MP2 Lagrangian:
\beq
<0|\op W \op T_2 |0> + \sum_{\mu_2} \tnsr \Lambda_{\mu_2} <\mu_2|\op W +\op F \op T_2 |0>_C
\eeq

OUTPUT FILE
-----------

Closed-shell expressions in spacial orbitals.
Occupied indices: ijklmno (syntax:occorb)
Virtual indices: abcdefgh (syntax:virorb)
General indices: pqrstuvwxyz (syntax:genorb)

4-index integrals are written in the chemical notation: (ai|bj)
All indices of amplitudes are written as subscript: T_{aibj} - first two indices correspond to electron 1, etc.
Amplitudes coming from deexcitation operators have opposite order of occupied and virtual indices, e.g.
T_{iajb}^{\dg} - again first two indices correspond to electron 1, etc. 


PERMUTATIONS

\Perm(<from>,<to>)
Indices in the expression have to be permuted, the first set of indices is "from", the second is "to", e.g.,
\Perm(ijk,jki)T_{aibjck} => T_{ajbkci}

It is possible to divide all expressions by the sum of permutations using `act,divide=$(<sum of permutations>)$, e.g.,
in order to derive spin-summed perturbative triples equations:
act,divide=$(1 - \Perm{abc}{cab})$
\beq
\bract \op H (\op T_2 ) |0>_C
\eeq
